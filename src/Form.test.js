import React from 'react'
import { Form } from './Form'
import { render, cleanup } from 'react-testing-library'

const generateQuestions = (questions, { type, radioValues }) => {
  return questions.map(i => ({
    name: `$input=${i}`,
    validate: [
      {
        type: 'isGreater',
        condition: 3,
      },
    ],
    type: {
      name: type,
      radioValues,
    },
  }))
}

describe('form builder', () => {
  afterEach(cleanup)
  test('renders text input according to manifest', () => {
    const questions = generateQuestions([1, 2, 3], { type: 'text' })
    const { getAllByLabelText } = render(<Form questions={questions} />)
    const label = getAllByLabelText(/input/)
    expect(label).toHaveLength(3)
  })
  test('renders radio input components according to manifest', () => {
    const questions = generateQuestions([1, 2, 3], { type: 'radio', radioValues: ['value1', 'value2'] })
    const { queryAllByRole } = render(<Form questions={questions} />)
    const radio = queryAllByRole('radio')
    expect(radio).toHaveLength(6)
  })
  test('renders customer text input component in place of standard text input', () => {
    const CustomTextInput = () => <input data-testid="test-text-input" />
    const questions = generateQuestions([1, 2, 3], { type: 'text' })
    const { getAllByLabelText, queryAllByTestId } = render(
      <Form questions={questions} customComponents={{ text: () => <CustomTextInput /> }} />,
    )

    const label = getAllByLabelText
    expect(() => label()).toThrowError()
    expect(queryAllByTestId('test-text-input')).toHaveLength(3)
  })
  test('renders customer radio input component in place of standard radio input', () => {
    const CustomRadioInput = () => <input data-testid="test-radio-input" />
    const questions = generateQuestions([1, 2, 3], { type: 'radio', radioValues: ['value1', 'value2'] })
    const { queryAllByRole, queryAllByTestId } = render(
      <Form questions={questions} customComponents={{ radio: () => <CustomRadioInput /> }} />,
    )

    const label = queryAllByRole('radio')
    expect(label).toHaveLength(0)

    expect(queryAllByTestId('test-radio-input')).toHaveLength(3)
  })
  test('renders custom compoments based on name type on manifest', () => {
    const CustomComponent = () => <input data-testid="test-custom-input" />
    const questions = generateQuestions([1, 2, 3], { type: 'custom' })
    const { queryAllByTestId } = render(
      <Form questions={questions} customComponents={{ custom: () => <CustomComponent /> }} />,
    )

    expect(queryAllByTestId('test-custom-input')).toHaveLength(3)
  })
  // test('generate error if custom name type on manifest does not match any customComponents ', () => {
  //   const CustomComponent = () => <input data-testid='test-custom-input' />
  //   const questions = generateQuestions([1], {type: 'custom'})
  //   const response = render(
  //       <Form questions={questions} customComponents={{radio: () => <CustomComponent />}} />)
  //   expect(() => response).toThrow('Component to render is not declared')
  // })
})
