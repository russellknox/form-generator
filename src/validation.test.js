import validation from './validation'

const generateValidationRule = (type, condition = null, error = false) => [
  {
    type,
    condition,
    error,
  },
]
const customErrorMessage = 'Custom error message'

describe('validation', () => {
  test('min length not satisfied', () => {
    const validate = generateValidationRule('min', 4)
    const response = validation(validate)('One')
    expect(response).toEqual({ error: 'Too short' })
  })
  test('min length satisfied', () => {
    const validate = generateValidationRule('min', 4)
    const response = validation(validate)('OneOne')
    expect(response).toEqual(false)
  })
  test('min length not satisfied with custom error message', () => {
    const validate = generateValidationRule('min', 4, customErrorMessage)
    const response = validation(validate)('One')
    expect(response).toEqual({ error: customErrorMessage })
  })
  test('max length not satisfied', () => {
    const validate = generateValidationRule('max', 4)
    const response = validation(validate)('OneOne')
    expect(response).toEqual({ error: 'Too long' })
  })
  test('max length satisfied', () => {
    const validate = generateValidationRule('max', 4)
    const response = validation(validate)('One')
    expect(response).toEqual(false)
  })
  test('max length satisfied with custom error message', () => {
    const validate = generateValidationRule('max', 4, customErrorMessage)
    const response = validation(validate)('OneOne')
    expect(response).toEqual({ error: customErrorMessage })
  })
  test('string type not satisfied', () => {
    const validate = generateValidationRule('string')
    const response = validation(validate)(123)
    expect(response).toEqual({ error: 'Needs to be a string' })
  })
  test('string type satisfied', () => {
    const validate = generateValidationRule('string')
    const response = validation(validate)('One')
    expect(response).toEqual(false)
  })
  test('string type not satisfied with custom error message', () => {
    const validate = generateValidationRule('string', null, customErrorMessage)
    const response = validation(validate)(123)
    expect(response).toEqual({ error: customErrorMessage })
  })
  test('number type not satisfied', () => {
    const validate = generateValidationRule('number')
    const response = validation(validate)('One')
    expect(response).toEqual({ error: 'Needs to be a number' })
  })
  test('number type satisfied', () => {
    const validate = generateValidationRule('number')
    const response = validation(validate)(123)
    expect(response).toEqual(false)
  })
  test('number type not satisfied with custom error message', () => {
    const validate = generateValidationRule('number', null, customErrorMessage)
    const response = validation(validate)('One')
    expect(response).toEqual({ error: customErrorMessage })
  })
  test('email not satisfied', () => {
    const validate = generateValidationRule('email')
    const response = validation(validate)('email')
    expect(response).toEqual({ error: 'Needs to be an email' })
  })
  test('email satisfied', () => {
    const validate = generateValidationRule('email')
    const response = validation(validate)('email@y.com')
    expect(response).toEqual(false)
  })
  test('email not satisfied with custom error message', () => {
    const validate = generateValidationRule('email', null, customErrorMessage)
    const response = validation(validate)('email')
    expect(response).toEqual({ error: customErrorMessage })
  })
  test('calls and executes custom validation and return error', () => {
    const validate = generateValidationRule('customMessage', null, customErrorMessage)
    const response = validation(validate, {
      customMessage: (input, rule) => {
        let errors = {}
        if (input !== 'customMessage') {
          errors.error = rule.error
          return errors
        }
        return false
      },
    })('notSameAsCustomMessage')
    expect(response).toEqual({ error: customErrorMessage })
  })
  test('calls and executes custom validation and return false when there is no error', () => {
    const validate = generateValidationRule('customMessage', null, customErrorMessage)
    const response = validation(validate, {
      customMessage: (input, rule) => {
        let errors = {}
        if (input !== 'sameAsCustomMessage') {
          errors.error = rule.error
          return errors
        }
        return false
      },
    })('sameAsCustomMessage')
    expect(response).toEqual(false)
  })
  // test('thorws error when custom validation function does not match type on manifest', () => {
  //     const validate = generateValidationRule('customMessage', null, customErrorMessage)
  //     const response = validation(validate, {
  //         inCustomMessage: () => {}
  //     })('sameAsCustomMessage')
  //     expect(response).toThrow('Custom validation does not match validation type on manifest')
  // })
})
