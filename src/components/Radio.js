import React from 'react'
import { Input } from './Input'

export const Radio = props => (
  <>
    {props.radioValues.map(field => (
      <div key={field}>
        <label htmlFor={props.name}>{field}</label>
        <Input role="radio" aria-checked id={props.name} name={field} {...props} value={field} autoComplete="on" />
      </div>
    ))}
  </>
)
