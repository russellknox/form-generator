import styled from 'styled-components'

const Input = styled('input')`
  border: 2px solid #5d5d5d;
  width: 200px;
  height: 30px;
  margin: 10px;
`

export { Input }
