import React from 'react'
import styled from 'styled-components'
import { Input } from './Input'

const Flex = styled.div`
  display: flex;
  flex-direction: column;
`

export const Text = props => (
  <Flex>
    <label htmlFor={props.name}>{props.name}</label>
    <Input id={props.name} className="thisisatest" {...props} />
  </Flex>
)
