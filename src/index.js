import React from 'react'
import ReactDOM from 'react-dom'
import { App } from './example/Form'

ReactDOM.render(<App />, document.getElementById('root'))
