import React from 'react'
import { Field } from 'formik'
import validation from './validation'
import * as components from './components'

import { getInputComponent } from './helpers'

const Fields = ({ questions, customComponents, customValidation, ...props }) => {
  return questions.map(({ name, validate, type }) => {
    return (
      <div key={name}>
        <Field name={name} validate={validation(validate, customValidation)}>
          {({ field, form }) => {
            const renderComponent = getInputComponent(components, customComponents)
            if (!renderComponent[type.name]) {
              throw new Error('Component to render is not declared')
            }
            return renderComponent[type.name]({ ...type, ...field, ...form })
          }}
        </Field>
        {props.touched[name] && props.errors[name] && <div>{props.errors[name].error}</div>}
      </div>
    )
  })
}

export { Fields }
