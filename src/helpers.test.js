import React from 'react'
import { getInputComponent, generateInitialValues } from './helpers'

describe('render correct component', () => {
  test('render standard text input component', () => {
    const Text = () => <div>Text</div>
    const response = getInputComponent({ Text }, {}).text({})
    expect(response).toEqual(<Text />)
  })
  test('render custom Text component when pass as prop', () => {
    const CustomComponent = () => {}
    const standardComponent = () => {}
    const response = getInputComponent({ Text: standardComponent }, { text: () => <CustomComponent /> }).text({})
    expect(response).toEqual(<CustomComponent />)
  })
  test('render custom Component when pass as prop and defined as question type name in manifest', () => {
    const CustomComponent = () => {}
    const standardComponent = () => {}
    const response = getInputComponent({ Text: standardComponent }, { custom: () => <CustomComponent /> }).custom({})
    expect(response).toEqual(<CustomComponent />)
  })
  test('render standard radio input component passing on radioValues', () => {
    const type = { radioValues: [1, 2] }
    const Radio = () => {}
    const response = getInputComponent({ Radio }, {}).radio({ ...type })
    expect(response).toEqual(<Radio radioValues={[1, 2]} />)
  })
  test('render custom radio component when pass as prop', () => {
    const type = { radioValues: [] }
    const CustomRadioComponent = () => {}
    const Radio = () => {}
    const response = getInputComponent({ Radio }, { radio: () => <CustomRadioComponent /> }).radio({ ...type })
    expect(response).toEqual(<CustomRadioComponent />)
    expect(response).not.toBe(Radio)
  })
  test('error thrown when no radioValue passed', () => {
    const response = getInputComponent({}, {}).radio
    expect(() => response({})).toThrowError('For radio buttons, you need to pass a value array containing values')
  })
  test('error thrown when no radioValue is not an array', () => {
    const type = { radioValues: '' }
    const response = getInputComponent({}, {}).radio
    expect(() => response({ type })).toThrowError('For radio buttons, you need to pass a value array containing values')
  })
})
describe('helpers functions', () => {
  test('generate object of empty values based on question names for Formik initial values', () => {
    const questions = [{ name: 'question1' }, { name: 'question2' }, { name: 'question3' }]
    const response = {
      question1: '',
      question2: '',
      question3: '',
    }
    expect(generateInitialValues(questions)).toEqual(response)
  })
})
