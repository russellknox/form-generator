import React from 'react'
import propTypes from 'prop-types'
import { Formik, Form as FormikForm } from 'formik'
import { Fields } from './Fields'
import { generateInitialValues } from './helpers'

const Form = ({ questions, customComponents = {}, customValidation = {}, onSubmit, asyncRender = false, children }) => (
  <Formik onSubmit={onSubmit} initialValues={generateInitialValues(questions)}>
    {props => (
      <FormikForm>
        <Fields
          questions={asyncRender ? [asyncRender] : questions}
          customComponents={customComponents}
          customValidation={customValidation}
          {...props}
        />
        {children}
      </FormikForm>
    )}
  </Formik>
)

Form.propTypes = {
  questions: propTypes.arrayOf(propTypes.object).isRequired,
  onSubmit: propTypes.func,
  asyncRender: propTypes.bool,
  customComponents: propTypes.object,
}

export { Form }
