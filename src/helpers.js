import React from 'react'

export const getInputComponent = ({ Text, Radio }, custom) => ({
  text: props => {
    const TextComponent = Text
    // const TextComponent = custom.Text ? custom.Text : Text
    return <TextComponent {...props} />
  },
  radio: props => {
    if (!props.radioValues || !Array.isArray(props.radioValues)) {
      throw new Error('For radio buttons, you need to pass a value array containing values')
    }
    // const RadioComponent = custom.Radio ? custom.Radio : Radio
    const RadioComponent = Radio
    return <RadioComponent {...props} />
  },
  ...custom,
})

// return initalValue based on questions passed in as props
export const generateInitialValues = questions => {
  return questions.reduce((state, acc) => ({ ...state, [acc.name]: '' }), {})
}
