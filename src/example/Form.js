import React from 'react'
import { Form } from '../Form'
import styled from 'styled-components'
import questions from './questions'

const Input = styled.input`
  border: red 2px solid;
  width: 300px;
  padding: 5px;
`

const customComponents = {
  custom: props => <Input placeholder='Custom validation. Input must === "validation"' {...props} />,
}

const customValidation = {
  custom: (input, rule) => {
    let errors = {}
    if (input !== 'validation') {
      errors.error = rule.error ? rule.error : 'Needs to equal "validation"'
      return errors
    }
    return false
  },
}

const App = () => (
  <Form
    customComponents={customComponents}
    customValidation={customValidation}
    questions={questions}
    onSubmit={a => alert(JSON.stringify(a))}
  >
    <input type="submit" />
  </Form>
)

export { App }
