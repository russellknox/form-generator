export default [
  {
    name: 'Username',
    validate: [
      {
        type: 'min',
        condition: 3,
      },
    ],
    type: {
      name: 'text',
    },
  },
  {
    name: 'Age',
    validate: [
      {
        type: 'number',
      },
    ],
    type: {
      name: 'text',
    },
  },
  {
    name: 'Email',
    type: {
      name: 'text',
    },
    validate: [
      {
        type: 'email',
      },
    ],
  },
  {
    name: 'custom',
    validate: [
      {
        type: 'custom',
      },
    ],
    type: {
      name: 'custom',
    },
  },
]
