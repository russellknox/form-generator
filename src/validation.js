import joi from 'joi'

const validationRules = custom => ({
  min: (input, rule) => {
    let errors = {}
    if (input.length < rule.condition) {
      errors.error = rule.error ? rule.error : 'Too short'
      return errors
    }
    return false
  },
  max: (input, rule) => {
    let errors = {}
    if (input.length > rule.condition) {
      errors.error = rule.error ? rule.error : 'Too long'
      return errors
    }
    return false
  },
  string: (input, rule) => {
    let errors = {}
    if (joi.validate(input, joi.string()).error) {
      errors.error = rule.error ? rule.error : 'Needs to be a string'
      return errors
    }
    return false
  },
  number: (input, rule) => {
    let errors = {}
    if (joi.validate(input, joi.number()).error) {
      errors.error = rule.error ? rule.error : 'Needs to be a number'
      return errors
    }
    return false
  },
  email: (input, rule) => {
    let errors = {}
    if (joi.validate(input, joi.string().email()).error) {
      errors.error = rule.error ? rule.error : 'Needs to be an email'
      return errors
    }
    return false
  },
  ...custom,
})

// input is provided from Formik Field validate function
const validation = (rules, custom = {}) => input => {
  // return object with multiple errors. Formik determines what input the error belongs too
  const validationErrors = rules.reduce((state, rule) => {
    const rules = validationRules(custom)[rule.type]

    if (!rules) {
      throw new Error('Custom validation does not match validation type on manifest')
    }

    return {
      ...state,
      ...rules(input, rule),
    }
  }, {})

  // return false if not validation error. Otherwise onSubmit will not happen
  if (Object.keys(validationErrors).length < 1) return false

  return validationErrors
}

export default validation
